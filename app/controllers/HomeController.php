<?php

class HomeController extends BaseController
{


    public function showWelcome()
    {
        $response = array(
            'version' => 1.0,
            'last-update' => '2014-11-20'
        );
        return Response::json($response);
    }

}

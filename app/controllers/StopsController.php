<?php


class StopsController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $stops = Stop::all();
        return $this->parseResponse($stops);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * * @param  int $stop
     * @return Response
     */
    public function show($text)
    {
        $stops = Stop::where('name', 'LIKE', '%' . $text . '%')->limit(50)->get();
        return $this->parseResponse($stops);
    }

    public function  map()
    {

        $input = Input::all();

        $lines = Stop::
        where('posx', '>', $input['rightbottom']['y'])->
        where('posx', '<', $input['lefttop']['y'])->
        where('posy', '<', $input['rightbottom']['x'])->
        where('posy', '>', $input['lefttop']['x'])->
        get();


        return $this->parseResponse($lines);
    }

    public function stopsNearBy($limit = 10)
    {


        if ($limit > 50) {
            $limit = 50;
        }

        $input = Input::all();

        //latitude = posx
        //longitude = posy
        //$lat = 50.01758259000001;
        //$lon = 19.89534733;

        if (empty($input['lat']) || empty($input['lon'])) {
            $proximitySearch = [];
        } else {
            $proximitySearch = DB::select("SELECT *,
        ( 6371 * acos( cos( radians(?) )
         * cos( radians( posx ) )
         * cos( radians( posy ) - radians(?) ) + sin( radians(?) )
         * sin( radians( posx ) ) ) ) AS distance
        FROM ro_stops HAVING distance < 10000
        ORDER BY distance LIMIT ?", array($input['lat'], $input['lon'], $input['lat'], $limit));
        }

        return $this->parseResponse($proximitySearch);

    }

    public function updateLocation()
    {
        $input = Input::all();

        if (!empty($input)) {
            $idGet = Stop::where('name', '=', $input['name'])->limit(1)->get();
            foreach ($idGet as $id) {
                $toSave = Stop::find($id->id);
                $toSave->posx = $input['posx'];
                $toSave->posy = $input['posy'];
                $toSave->save();
            }
            print_r($input);
        }


        return View::make('simpleform');
    }

    private function parseResponse($stops)
    {
        $statusCode = 200;
        $response = [];
        foreach ($stops as $stop) {
            if (empty($stop->distance)) {
                $stop->distance = -1;
            }
            $response[] = [
                'id' => $stop->id,
                'name' => $stop->name,
                'slug' => $stop->slug,
                'latitude' => $stop->posx,
                'longitude' => $stop->posy,
                'distance' => $stop->distance,
                'options' => array(
                    'labelContent' => $stop->name,
                    'labelClass' => 'map-marker-label'
                )

            ];
        }

        return Response::json($response, $statusCode);
    }


    public function strings()
    {
        $stops = Stop::all();
        $string = "";
        foreach ($stops as $stop) {

            $string = $string . ",'" . $stop->name . "'";


        }
        echo substr($string, 1);
    }
}
<?php

use Goutte\Client;
use Illuminate\Support\Str;

class CrawlController extends BaseController
{

    public $lines = array();
    /**
     * @var Client
     */
    public $client;
    public $crawler;
    public $stopTemplate = "http://rozklady.mpk.krakow.pl/aktualne/%s/%st%s.htm";

    //current line
    public $currentLineID;


    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Starts initial crawl process
     * generates all lines format that matches MPK Krakow names scheme
     */
    public function getLines()
    {

        //DAYFLAGS
        //0 - normal day
        //1 - saturday
        //2 - holiday

        $lines = Line::get(array('number'));

        return Response::json($lines);
    }


    public function queue()
    {
        return View::make('queue');
    }

    public function clearDatabase()
    {
        Line::truncate();
        TramRoute::truncate();
        Stop::truncate();
        Departures::truncate();

        return Response::json('true');
    }

    public function saveLines()
    {
        $lines = [];
        $this->crawler = $this->client->request('GET', 'http://rozklady.mpk.krakow.pl/linie.aspx');
        $this->crawler->filter('a')->each(function ($node) use (&$lines) {
            if (strlen($node->text()) < 4) {
                $lines[] = array('number' => $node->text());
            }
        });
        Line::insert($lines);
        return Response::json('true');
    }

    public function saveStops()
    {

        $this->crawler = $this->client->request('GET', 'http://rozklady.mpk.krakow.pl/aktualne/przystan.htm');
        $stops = [];
        $this->crawler->filter('li')->each(function ($node) use (&$stops) {
            $stops[] = array(
                'name' => mb_strtoupper($node->text()),
                'slug' => Str::slug($node->text()),
                'posx' => '',
                'posy' => ''
            );
        });
        try {

            Stop::insert($stops);
        } catch (Exception $e) {

        }

        return Response::json('true');
    }

    public function saveDepartures($line)
    {

        $processedLine = Line::where('number', '=', $line)->take(1)->get();

        foreach ($processedLine as $lineNumber) {
            $singleLine = $this->singleLine($lineNumber->number);

            $routes = $this->saveRoute($singleLine['data']);
            $currentLine = $lineNumber->id;

            foreach ($singleLine['data'] as $singleStop) {

                if ($singleStop['route'][0] == $routes[0]['start_name'] && $singleStop['route'][2] == $routes[0]['end_name']) {
                    $currentRoute = $routes[0]['id'];
                } else {
                    $currentRoute = $routes[1]['id'];
                }

                $currentStop = Stop::where('name', '=', mb_strtoupper($singleStop['stop']))->take(1)->get();
                $currentStop = $currentStop[0]->id;

                $departureModel = $this->parseDeparturesForSave($currentLine, $currentRoute, $currentStop, $singleStop['departures']);

                Departures::insert($departureModel);
            }
        }

        return Response::json('true');
    }

    public function parseDeparturesForSave($currentLine, $currentRoute, $currentStop, $data)
    {

        $insertArray = array();
        foreach ($data as $hour) {
            $i = 0;
            foreach ($hour as $singleHour) {
                if (is_numeric($singleHour[1][0])) {
                    foreach ($singleHour[1] as $exactTime) {
                        $insertArray[] = array(
                            'hour' => $singleHour[0],
                            'minute' => $exactTime,
                            'hhmm' => $singleHour[0] . ':' . $exactTime,
                            'dayflag' => $i,
                            'stop_id' => $currentStop,
                            'line_id' => $currentLine,
                            'route_id' => $currentRoute

                        );
                    }
                }
                if ($i++ > 2) {
                    $i = 0;
                }
            }
        }


        return $insertArray;
    }

    public function saveRoute($data)
    {
        $noOfStops = count($data);

        $fromIDBegin = Stop::where('name', '=', $data[0]['route'][0])->take(1)->get();
        $toIDBegin = Stop::where('name', '=', $data[0]['route'][2])->take(1)->get();

        $fromIDEnd = Stop::where('name', '=', $data[$noOfStops - 1]['route'][0])->take(1)->get();
        $toIDEnd = Stop::where('name', '=', $data[$noOfStops - 1]['route'][2])->take(1)->get();

        $routes = array(
            0 => array(
                'start_name' => $data[0]['route'][0],
                'end_name' => $data[0]['route'][2],
                'start' => $fromIDBegin[0]->id,
                'end' => $toIDBegin[0]->id
            ),
            1 => array(
                'start_name' => $data[$noOfStops - 1]['route'][0],
                'end_name' => $data[$noOfStops - 1]['route'][2],
                'start' => $fromIDEnd[0]->id,
                'end' => $toIDEnd[0]->id
            )
        );


        foreach ($routes as &$oneRoute) {
            $routeModel = TramRoute::whereRaw('start_id = ? and end_id = ?', array($oneRoute['start'], $oneRoute['end']))->get();
            $continue = true;
            $route = null;
            foreach ($routeModel as $tmpModel) {
                $continue = false;
                $route = $tmpModel->id;
            }

            if ($continue) {
                $tramRouteModel = new TramRoute();
                $tramRouteModel->start_id = $oneRoute['start'];
                $tramRouteModel->end_id = $oneRoute['end'];
                $tramRouteModel->save();
                $route = $tramRouteModel->id;
            }
            $oneRoute['id'] = $route;
        }

        return $routes;
    }

    public function singleLine($number, $parseDepartures = true)
    {
        $number = $this->parseZeroEndings($number);
        $status = 200;
        $stops = array();
        $stopCount = 1;
        do {
            try {

                $this->client->restart();
                $link = sprintf($this->stopTemplate, $number, $number, $this->parseZeroEndings($stopCount++, 3));

                $this->crawler = $this->client->request('GET', $link);
                $status = $this->client->getResponse()->getStatus();
                if ($status != 200) {
                    break;
                }

                $table = $this->crawler->filter('.tabletiming');
                //name of a single tram/bus stop
                $stop = $table->filter('.cellstop .fontstop')->text();

                //extract direction in which the vehicle is traveling to
                $route = $table->filter('.cellstop .fontroute')->text();
                $route = trim(substr($route, 7));
                $route = explode(' - ', $route);
                unset($route[1]);

                //extract departures hours and minutes
                $departures = ($parseDepartures ? $this->getSingleLineDepartures($table->filter('.celldepart')) : array());

                $stops[] = array(
                    'route' => $route,
                    'stop' => $stop,
                    'departures' => $departures
                );


            } catch (Exception $e) {
                Debugbar::error($e);
            }

        } while (true);

        $information = array(
            'line' => $number,
            'data' => $stops
        );
        return $information;


    }

    /**
     * @param $table \Symfony\Component\DomCrawler\Crawler
     * @return array
     */
    private function getSingleLineDepartures($table)
    {
        $rows = array();


        $table->filter('td')->each(function ($node2) use (&$rows, &$i) {
            $text = trim($node2->text());
            if (strlen($text) < 90) {

                $rows[] = $text;
            }
        });
        $days = 0;

        while (!is_numeric($rows[$days])) {
            unset($rows[$days]);
            $days++;
        }


        $tmp = array();
        for ($i = 0; $i < $days; $i++) {
            $tmp[$i] = array();
        }

        $hours = array();

        $i = 0;
        $z = 0;
        $k = 0;

        foreach ($rows as $row) {
            $i++;


            if ($i % 2 != 0) {
                $tmp[$k][$z] = $row;
            } else {
                $tmp[$k][$z] = explode(' ', $row);
                $k++;
            }


            if ($z++ >= 1) {
                $z = 0;
            }

            if ($k > $days - 1) {
                $k = 0;
            }

            if ($i > $days * 2 - 1) {
                $i = 0;
                $hours[$tmp[0][0]] = $tmp;
                for ($p = 0; $p < $days; $p++) {
                    $tmp[$p] = array();
                }
            }


        }


        return $hours;
    }

    private function parseZeroEndings($number, $lenght = 4)
    {
        return str_pad($number, $lenght, '0', STR_PAD_LEFT);
    }

}
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
header('Access-Control-Allow-Credentials: true');

Route::get('/', array('uses' => 'HomeController@showWelcome'));


Route::group(array('prefix' => 'api/v1'), function () {

    Route::resource('lines', 'LinesController',
        array('only' => array('index', 'show')));
    Route::resource('stops', 'StopsController',
        array('only' => array('index', 'show')));
    Route::resource('departures', 'DeparturesController',
        array('only' => array('show')));
    Route::get('departures/{id}/{number}', array('uses' => 'DeparturesController@show'));
    Route::match(array('GET', 'POST'), 'update-loc', array('uses' => 'StopsController@updateLocation'));
    Route::match(array('GET', 'POST'), 'get-markers', array('uses' => 'StopsController@map'));
    Route::match(array('GET', 'POST'), 'close-to-me/{limit?}', array('uses' => 'StopsController@stopsNearBy'));
});


App::missing(function ($exception) {
    return Redirect::to('/');
});

//Route::group(array('prefix' => 'crawl'), function () {
//
//    Route::get('/', array('uses' => 'CrawlController@welcome'));
//    Route::get('/clear', array('uses' => 'CrawlController@clearDatabase'));
//    Route::get('/lines', array('uses' => 'CrawlController@saveLines'));
//    Route::get('/stops', array('uses' => 'CrawlController@saveStops'));
//    Route::get('/list-lines', array('uses' => 'CrawlController@getLines'));
//    Route::get('/queue', array('uses' => 'CrawlController@queue'));
//    Route::get('/departures/{line}', array('uses' => 'CrawlController@saveDepartures'));
//
//});




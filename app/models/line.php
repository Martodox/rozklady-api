<?php



class Line extends Eloquent
{
    public $timestamps = false;

    protected $fillable = array('number');

    public function departure()
    {
        return $this->belongsTo('Departures');
    }

}